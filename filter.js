const blobServiceClient = require("./client");

const containerName = "test-sentinela";

async function getProperties(blobClient) {

    const properties = await blobClient.getProperties();
    console.log(blobClient.name + ' properties: ');
  
    for (const property in properties) {
  
      switch (property) {
        // nested properties are stringified and returned as strings
        case 'metadata':
        case 'objectReplicationRules':
          console.log(`    ${property}: ${JSON.stringify(properties[property])}`);
          break;
        default:
          console.log(`    ${property}: ${properties[property]}`);
          break;
      }
    }
  }

async function listBlobsInsideAContainer() {
  const containerClient = blobServiceClient.getContainerClient(containerName);


  let i = 1;
  let blobs = containerClient.listBlobsFlat();

  //criar o filtro
  //const filteredBlobs = blobs.filter((blob)=>{
  //  if( new Date() > Date(blob.properties.lastModified))
  //})

  for await (const blob of blobs) {
    console.log(blob);
    console.log(`Blob ${i++}: ${blob.name}`);
  }
}

listBlobsInsideAContainer();

